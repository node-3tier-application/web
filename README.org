#+TITLE:Devops Web App
#+DATE: Monday, Oct 14 2019

** install the node packages for the web tier:
	#+begin_src bash
	→ npm install
	#+end_src

** start the app
   #+begin_src 
   → npm start   
   #+end_src

**  NOTE this app uses two env variables:
   Dependent environment variables:
   
    - PORT: the listening PORT
    - API_HOST: the full url to call the API app

    These two variables need to be set 
