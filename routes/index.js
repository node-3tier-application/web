var express = require('express');
var os = require('os');
var router = express.Router();
var request = require('request');

var apiUrl = process.env.API_HOST + '/api/status';

// healthcheck, to route traffic to healty hosts
router.get('/healthz', function (req, res) {
  return res.status(200).json({
    status: 'still alive ;)'
  });
});

// TODO: #reviselater, lb always send http traffic to pods, so request will circle around loop, if we do this way
// // HTTP to HTTPS redirection if NODE_ENV=production
// // credits: http://heyrod.com/snippets/redirect-http-to-https-in-expressjs.html
// var HTTPS_PORT = 443;
// router.all('/*', function (req, res, next) {
//   if (/^http$/.test(req.protocol) && process.env.NODE_ENV === 'production') {
//     var host = req.headers.host.replace(/:[0-9]+$/g, ''); // strip the port # if any
//     if ((HTTPS_PORT != null) && HTTPS_PORT !== 443) {
//       return res.redirect('https://' + host + ':' + HTTPS_PORT + req.url, 301);
//     } else {
//       return res.redirect('https://' + host + req.url, 301);
//     }
//   } else {
//     return next();
//   }
// });

/* GET home page. */
router.get('/', function (req, res, next) {
  request(
    {
      method: 'GET',
      url: apiUrl,
      json: true
    },
    function (error, response, body) {
      if (error || response.statusCode !== 200) {
        return res.status(500).send('error running request to ' + apiUrl);
      } else {
        res.render('index', {
          title: '3tier App',
          request_uuid: body.request_uuid,
          time: body.time,
          hostname: os.hostname(),
          cdnUrl: process.env.CDN_URL ? process.env.CDN_URL : ''
        });
      }
    }
  );
});

router.get('/error', (req, res, next) => {
  res.send('Something broke!');
  next(new Error('Custom error message'));
});

module.exports = router;
