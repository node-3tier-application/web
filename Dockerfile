FROM node:alpine


RUN apk update \
    && apk add --no-cache --virtual .ops-deps \
    curl busybox-extras \
    postgresql-client

RUN adduser deploy -D
USER deploy

WORKDIR /home/deploy
COPY . /home/deploy

RUN npm install
ENTRYPOINT ["npm"]
CMD ["start"]
